// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_task.h"
/* ========================================================================== */
// DAQmxClearTask
// [status] = DAQ_ClearTask( taskHandle )
DAQ_TASK_1( DAQ_ClearTask, DAQmxClearTask );
/* ========================================================================== */
// DAQmxStartTask
// [status] = DAQ_StartTask( taskHandle )
DAQ_TASK_1( DAQ_StartTask, DAQmxStartTask );
/* ========================================================================== */
// DAQmxStopTask
// [status] = DAQ_StopTask( taskHandle )
DAQ_TASK_1( DAQ_StopTask, DAQmxStopTask );
/* ========================================================================== */
// DAQmxCreateTask
// [taskHandle, status] = DAQ_CreateTask( taskName )
DAQ_TASK_2( DAQ_CreateTask, DAQmxCreateTask );
/* ========================================================================== */
// DAQmxLoadTask
// [taskHandle, status] = DAQ_LoadTask( taskName )
DAQ_TASK_2( DAQ_LoadTask, DAQmxLoadTask );
/* ========================================================================== */
