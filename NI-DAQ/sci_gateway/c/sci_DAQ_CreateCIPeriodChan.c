// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_channel.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxCreateCIPeriodChan
// [status] = DAQ_CreateCIPeriodChan( taskHandle, counter,
//                                    minVal, maxVal, units, edge )
//
int sci_DAQ_CreateCIPeriodChan( char* fname )
{
  TaskHandle taskHandle = 0;
  char *counter = NULL;
  float64 minVal = 0.0;
  float64 maxVal = 0.0;
  int32 units = 0;
  int32 edge = 0;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 6, 6 );
  CheckLhs( 1, 1 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsString( 2, counter, m, n, l );
  GetRhsFloat64( 3, minVal, m, n, l );
  GetRhsFloat64( 4, maxVal, m, n, l );
  GetRhsInt32( 5, units, m, n, l );
  GetRhsInt32( 6, edge, m, n, l );

  status = DAQmxCreateCIPeriodChan( taskHandle,
                                        counter,
                                        NULL,
                                        minVal,
                                        maxVal,
                                        units,
                                        edge,
                                        DAQmx_Val_LowFreq1Ctr,
                                        0,
                                        1,
                                        NULL );

  CreateInt32( 7, status, m, n, l );

  LhsVar(1) = 7;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
