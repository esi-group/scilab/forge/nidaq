// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_channel.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxCreateAIThrmcplChan
// [status] = DAQ_CreateAIThrmcplChan( taskHandle, physicalChannel,
//                                      minVal, maxVal, units,
//                                      thermocoupleType,
//                                      cjcSource, cjcVal, cjcChannel )
//
int sci_DAQ_CreateAIThrmcplChan( char* fname )
{
  TaskHandle taskHandle = 0;
  char *physicalChannel = NULL;
  float64 minVal = 0.0;
  float64 maxVal = 0.0;
  int32 units = 0;
  int32 thermocoupleType = 0;
  int32 cjcSource = 0;
  float64 cjcVal = 0.0;
  char *cjcChannel = NULL;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 9, 9 );
  CheckLhs( 1, 1 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsString( 2, physicalChannel, m, n, l );
  GetRhsFloat64( 3, minVal, m, n, l );
  GetRhsFloat64( 4, maxVal, m, n, l );
  GetRhsInt32( 5, units, m, n, l );
  GetRhsInt32( 6, thermocoupleType, m, n, l );
  GetRhsInt32( 7, cjcSource, m, n, l );
  GetRhsFloat64( 8, cjcVal, m, n, l );
  GetRhsString( 9, cjcChannel, m, n, l );

  status = DAQmxCreateAIThrmcplChan( taskHandle,
                                          physicalChannel,
                                          NULL,
                                          minVal,
                                          maxVal,
                                          units,
                                          thermocoupleType,
                                          cjcSource,
                                          cjcVal,
                                          cjcChannel );

  CreateInt32( 10, status, m, n, l );

  LhsVar(1) = 10;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
