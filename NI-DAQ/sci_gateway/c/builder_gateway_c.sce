// Allan CORNET - DIGITEO - 2010

function builder_gw_c()

  // PutLhsVar managed by user in sci_sum and in sci_sub
  // if you do not this variable, PutLhsVar is added
  // in gateway generated (default mode in scilab 4.x and 5.x)
  WITHOUT_AUTO_PUTLHSVAR = %T;
  
  gw_files = ['sci_DAQ_WriteDigitalScalarU32.c', ..
              'sci_DAQ_WriteAnalogF64.c', ..
              'sci_daq_write.c', ..
              'sci_daq_trigger.c', ..
              'sci_daq_task.c', ..
              'sci_daq_systeminfo.c', ..
              'sci_DAQ_Success.c', ..
              'sci_DAQ_ResetDevice.c', ..
              'sci_DAQ_ReadCounterU32.c', ..
              'sci_DAQ_ReadCounterScalarF64.c', ..
              'sci_DAQ_ReadCounterF64.c', ..
              'sci_DAQ_ReadAnalogF64.c', ..
              'sci_daq_read.c', ..
              'sci_daq_p.c', ..
              'sci_daq_n_s.c', ..
              'sci_DAQ_IsTaskDone.c', ..
              'sci_DAQ_GetExtendedErrorInfo.c', ..
              'sci_DAQ_Failed.c', ..
              'sci_DAQ_CreateCOPulseChanFreq.c', ..
              'sci_DAQ_CreateCIPulseWidthChan.c', ..
              'sci_DAQ_CreateCIPeriodChan.c', ..
              'sci_DAQ_CreateCICountEdgesChan.c', ..
              'sci_DAQ_CreateAOVoltageChan.c', ..
              'sci_DAQ_CreateAIVoltageChan.c', ..
              'sci_DAQ_CreateAIThrmcplChan.c', ..
              'sci_daq_channel.c', ..
              'sci_DAQ_CfgSampClkTiming.c', ..
              'sci_DAQ_CfgInputBuffer.c', ..
              'sci_DAQ_CfgImplicitTiming.c', ..
              'sci_DAQ_CfgDigEdgeStartTrig.c', ..
              'sci_DAQ_CfgDigEdgeRefTrig.c', ..
              'sci_DAQ_CfgAnlgEdgeStartTrig.c', ..
              'sci_DAQ_CfgAnlgEdgeRefTrig.c', ..
              'sci_DAQ.c'];

  LDFLAGS = "";
  INCLUDES_NIDAQ = "";
  
  if getos() == 'Windows' then
    gw_files = [gw_files, 'dllMain.c'];

    NIEXTCCOMPILERSUPP = getenv('NIEXTCCOMPILERSUPP', '');
    if NIEXTCCOMPILERSUPP == '' then
      error(999,'NIEXTCCOMPILERSUPP variable environment not defined');
    end

    INCLUDES_NIDAQ = getshortpathname(fullpath(NIEXTCCOMPILERSUPP + filesep() + 'include'));

    if win64() then
      LIBPATH_NIDAQ = getshortpathname(fullpath(NIEXTCCOMPILERSUPP + filesep() + 'lib64' + filesep() + 'msvc'));
    else
      LIBPATH_NIDAQ = getshortpathname(fullpath(NIEXTCCOMPILERSUPP + filesep() + 'lib32' + filesep() + 'msvc'));
    end
    
    INCLUDES_NIDAQ = "-I""" + INCLUDES_NIDAQ + """";
    
    LDFLAGS = "/LIBPATH:""" + LIBPATH_NIDAQ + """";
    
  end
  
  gw_table = ['DAQ', 'sci_DAQ'; ..
         'DAQ_Failed', 'sci_DAQ_Failed'; ..
         'DAQ_Success', 'sci_DAQ_Success'; ..
         'DAQ_CfgAnlgEdgeRefTrig', 'sci_DAQ_CfgAnlgEdgeRefTrig'; ..
         'DAQ_CfgAnlgEdgeStartTrig', 'sci_DAQ_CfgAnlgEdgeStartTrig'; ..
         'DAQ_CfgDigEdgeRefTrig', 'sci_DAQ_CfgDigEdgeRefTrig'; ..
         'DAQ_CfgDigEdgeStartTrig', 'sci_DAQ_CfgDigEdgeStartTrig'; ..
         'DAQ_CfgImplicitTiming', 'sci_DAQ_CfgImplicitTiming'; ..
         'DAQ_CfgInputBuffer', 'sci_DAQ_CfgInputBuffer'; ..
         'DAQ_CfgSampClkTiming', 'sci_DAQ_CfgSampClkTiming'; ..
         'DAQ_ClearTask', 'sci_DAQ_ClearTask'; ..
         'DAQ_CreateAIThrmcplChan', 'sci_DAQ_CreateAIThrmcplChan'; ..
         'DAQ_CreateAIVoltageChan', 'sci_DAQ_CreateAIVoltageChan'; ..
         'DAQ_CreateAOVoltageChan', 'sci_DAQ_CreateAOVoltageChan'; ..
         'DAQ_CreateCICountEdgesChan', 'sci_DAQ_CreateCICountEdgesChan'; ..
         'DAQ_CreateCIPeriodChan', 'sci_DAQ_CreateCIPeriodChan'; ..
         'DAQ_CreateCIPulseWidthChan', 'sci_DAQ_CreateCIPulseWidthChan'; ..
         'DAQ_CreateCOPulseChanFreq', 'sci_DAQ_CreateCOPulseChanFreq'; ..
         'DAQ_CreateDIChan', 'sci_DAQ_CreateDIChan'; ..
         'DAQ_CreateDOChan', 'sci_DAQ_CreateDOChan'; ..
         'DAQ_CreateTask', 'sci_DAQ_CreateTask'; ..
         'DAQ_DisableRefTrig', 'sci_DAQ_DisableRefTrig'; ..
         'DAQ_DisableStartTrig', 'sci_DAQ_DisableStartTrig'; ..
         'DAQ_GetExtendedErrorInfo', 'sci_DAQ_GetExtendedErrorInfo'; ..
         'DAQ_IsTaskDone', 'sci_DAQ_IsTaskDone'; ..
         'DAQ_LoadTask', 'sci_DAQ_LoadTask'; ..
         'DAQ_ReadAnalogF64', 'sci_DAQ_ReadAnalogF64'; ..
         'DAQ_ReadBinaryI16', 'sci_DAQ_ReadBinaryI16'; ..
         'DAQ_ReadBinaryI32', 'sci_DAQ_ReadBinaryI32'; ..
         'DAQ_ReadCounterF64', 'sci_DAQ_ReadCounterF64'; ..
         'DAQ_ReadCounterScalarF64', 'sci_DAQ_ReadCounterScalarF64'; ..
         'DAQ_ReadCounterScalarU32', 'sci_DAQ_ReadCounterScalarU32'; ..
         'DAQ_ReadCounterU32', 'sci_DAQ_ReadCounterU32'; ..
         'DAQ_ReadDigitalScalarU32', 'sci_DAQ_ReadDigitalScalarU32'; ..
         'DAQ_ReadDigitalU32', 'sci_DAQ_ReadDigitalU32'; ..
         'DAQ_ReadDigitalU8', 'sci_DAQ_ReadDigitalU8'; ..
         'DAQ_ResetDevice', 'sci_DAQ_ResetDevice'; ..
         'DAQ_StartTask', 'sci_DAQ_StartTask'; ..
         'DAQ_StopTask', 'sci_DAQ_StopTask'; ..
         'DAQ_WriteAnalogF64', 'sci_DAQ_WriteAnalogF64'; ..
         'DAQ_WriteDigitalScalarU32', 'sci_DAQ_WriteDigitalScalarU32'; ..
         'DAQ_WriteDigitalU32', 'sci_DAQ_WriteDigitalU32'; ..
         'DAQ_WriteDigitalU8', 'sci_DAQ_WriteDigitalU8'; ..
         'DAQ_SystemInfoAttribute', 'sci_DAQ_SystemInfoAttribute'; ..
         '%daq_p', 'sci_daq_p'; ..
         '%daq_n_s', 'sci_daq_n_s'];


  tbx_build_gateway("gw_NIDAQ_c", ..
                    gw_table, ..
                    gw_files, ..
                    get_absolute_file_path("builder_gateway_c.sce"), ..
                    "", ..
                    LDFLAGS, ..
                    INCLUDES_NIDAQ);

endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
