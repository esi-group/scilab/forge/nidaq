// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011  Allan CORNET - DIGITEO
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq.h"
#include "MALLOC.h" // MALLOC & FREE
#include "Scierror.h"
/* ========================================================================== */
// DAQmxGetExtendedErrorInfo
// [errorString, status] = DAQ_GetExtendedErrorInfo()
int sci_DAQ_GetExtendedErrorInfo( char* fname )
{
  char *errorString = NULL;
  uInt32 bufferSize = 0;
  int32 status = 0;
  int m = 0, n = 0, l = 0;

  CheckRhs( 0, 0 );
  CheckLhs( 1, 2 );

  // determine bufferSize
    bufferSize = DAQmxGetExtendedErrorInfo( NULL, 0 );

    errorString = (char *) MALLOC( bufferSize * sizeof( char ) );
    if ( errorString == NULL) {
      Scierror( 999, "%s: malloc failed!", fname );
      return 0;
  }

  status = DAQmxGetExtendedErrorInfo( errorString, bufferSize );

  CreateString( 1, errorString, m, n, l );
  FREE( errorString );

  CreateInt32( 2, status, m, n, l );

  LhsVar( 1 ) = 1;
  LhsVar( 2 ) = 2;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
