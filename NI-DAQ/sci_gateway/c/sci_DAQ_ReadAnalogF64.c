// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_read.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxReadAnalogF64
// [readArray, sampsPerChanRead, status] =
// DAQ_ReadAnalogF64( taskHandle, numSampsPerChan, timeout, fillMode,
//                    arraySizeInSamps )
int sci_DAQ_ReadAnalogF64( char* fname )
{
  TaskHandle taskHandle = 0;
  int32 numSampsPerChan = 0;
  float64 timeout = 0.0;
  bool32 fillMode = 0;
  float64 *readArray = NULL;
  uInt32 arraySizeInSamps = 0;
  int32 sampsPerChanRead = 0;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 5, 5 );
  CheckLhs( 1, 3 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsInt32(2, numSampsPerChan, m, n, l );
  GetRhsFloat64( 3, timeout, m, n, l );
  GetRhsInt32( 4, fillMode, m, n, l );
  GetRhsUInt32( 5, arraySizeInSamps, m, n, l );

  m = 1;
  n = arraySizeInSamps;
  CreateVar( 6, MATRIX_OF_DOUBLE_DATATYPE ,&m ,&n , &l);
  readArray = stk( l );

  status = DAQmxReadAnalogF64( taskHandle, numSampsPerChan,
                                   timeout, fillMode,
                                   readArray, arraySizeInSamps,
                                   &sampsPerChanRead,
                                   ( bool32* )NULL );

  CreateInt32( 7, sampsPerChanRead, m, n, l );
  CreateInt32( 8, status, m, n, l );

  LhsVar( 1 ) = 6;
  LhsVar( 2 ) = 7;
  LhsVar( 3 ) = 8;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
