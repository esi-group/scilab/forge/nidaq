// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_channel.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxCreateDIChan
// [ status ] = DAQ_CreateDIChan (taskHandle, lines )
DAQ_CHANNEL_1( DAQ_CreateDIChan, DAQmxCreateDIChan );
/* ========================================================================== */
// DAQmxCreateDOChan
// [ status ] = DAQ_CreateDOChan (taskHandle, lines )
DAQ_CHANNEL_1( DAQ_CreateDOChan, DAQmxCreateDOChan );
/* ========================================================================== */
