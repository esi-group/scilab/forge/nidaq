// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_write.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxWriteDigitalScalarU32
// [status] =
//  DAQ_WriteDigitalScalarU32( taskHandle, autoStart, timeout, value )
int sci_DAQ_WriteDigitalScalarU32( char* fname )
{
  TaskHandle taskHandle = 0;
  bool32 autoStart = FALSE;
  float64 timeout = 0.0;
  uInt32 value = 0;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 4, 4 );
  CheckLhs( 1, 1 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsBool32( 2, autoStart, m, n, l );
  GetRhsFloat64( 3, timeout, m, n, l );
  GetRhsUInt32( 4, value, m, n, l );

  status = DAQmxWriteDigitalScalarU32( taskHandle,  autoStart,
                                           timeout, value,
                                           ( bool32* ) NULL );

  CreateInt32( 5, status, m, n, l );

  LhsVar( 1 ) = 5;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
