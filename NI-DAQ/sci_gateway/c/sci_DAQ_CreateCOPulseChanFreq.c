// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_channel.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxCreateCOPulseChanFreq
// [status] = DAQ_CreateCOPulseChanFreq( taskHandle, counter,
//                                       idleState, initialDelay,
//                                       freq, dutyCycle )
//
int sci_DAQ_CreateCOPulseChanFreq( char* fname )
{ 
  TaskHandle taskHandle = 0;
  char *counter = NULL;
  int32 idleState = 0;
  float64 initialDelay = 0.0;
  float64 freq = 0.0;
  float64 dutyCycle = 0.0;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 6, 6 );
  CheckLhs( 1, 1 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsString( 2, counter, m, n, l );
  GetRhsInt32( 3, idleState, m, n, l );
  GetRhsFloat64( 4, initialDelay, m, n, l );
  GetRhsFloat64( 5, freq, m, n, l );
  GetRhsFloat64( 6, dutyCycle, m, n, l );

  status = DAQmxCreateCOPulseChanFreq( taskHandle,
                                           counter,
                                           NULL,
                                           DAQmx_Val_Hz,
                                           idleState,
                                           initialDelay,
                                           freq,
                                           dutyCycle );

  CreateInt32( 7, status, m, n, l );

  LhsVar(1) = 7;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */
