// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_read.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxReadBinaryI16
// [readArray, sampsPerChanRead, status] =
// DAQ_ReadBinaryI16( taskHandle, numSampsPerChan, timeout, fillMode,
//                    arraySizeInSamps )
//
DAQ_READ_1( DAQ_ReadBinaryI16, INT16, DAQmxReadBinaryI16, int16 );
/* ========================================================================== */
// DAQmxReadBinaryI32
// [readArray, sampsPerChanRead, status] =
// DAQ_ReadBinaryI32( taskHandle, numSampsPerChan, timeout, fillMode,
//                    arraySizeInSamps )
//
DAQ_READ_1( DAQ_ReadBinaryI32, INT32, DAQmxReadBinaryI32, int32 );
/* ========================================================================== */
// DAQmxReadCounterScalarU32
// [value, status] = DAQ_ReadCounterScalarU32( taskHandle, timeout )
DAQ_READ_2( DAQ_ReadCounterScalarU32, DAQmxReadCounterScalarU32 );
/* ========================================================================== */
// DAQmxReadDigitalScalarU32
// [value, status] = DAQ_ReadDigitalScalarU32( taskHandle, timeout )
DAQ_READ_2( DAQ_ReadDigitalScalarU32, DAQmxReadDigitalScalarU32 );
/* ========================================================================== */
// DAQmxReadDigitalU32
// [readArray, sampsPerChanRead, status] =
// DAQ_ReadDigitalU32( taskHandle, numSampsPerChan, timeout, fillMode,
//                     arraySizeInSamps )
//
DAQ_READ_1( DAQ_ReadDigitalU32, UINT32, DAQmxReadDigitalU32, uInt32 );
/* ========================================================================== */
// DAQmxReadDigitalU8
// [readArray, sampsPerChanRead, status] =
// DAQ_ReadDigitalU8( taskHandle, numSampsPerChan, timeout, fillMode,
//                    arraySizeInSamps )
//
DAQ_READ_1( DAQ_ReadDigitalU8, UCHAR, DAQmxReadDigitalU8, uInt8 );
/* ========================================================================== */
