// DAQ Toolbox
// Copyright (C) 2008  Dirk Reusch, Kybernetik Dr. Reusch
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
/* ========================================================================== */
#include "sci_daq_channel.h"
#include "Scierror.h"
/* ========================================================================== */
// DAQmxCreateAOVoltageChan
// [status] = DAQ_CreateAOVoltageChan( taskHandle, physicalChannel,
//                                      minVal, maxVal )
//
int sci_DAQ_CreateAOVoltageChan( char* fname )
{
  TaskHandle taskHandle = 0;
  char *physicalChannel = NULL;
  float64 minVal = 0.0;
  float64 maxVal = 0.0;
  int32 status = 0;

  int m = 0, n = 0, l = 0;

  CheckRhs( 4, 4 );
  CheckLhs( 1, 1 );

  GetRhsTaskHandle( 1, taskHandle,  m, n, l );
  GetRhsString( 2, physicalChannel, m, n, l );
  GetRhsFloat64( 3, minVal, m, n, l );
  GetRhsFloat64( 4, maxVal, m, n, l );

  status = DAQmxCreateAOVoltageChan( taskHandle,
                                         physicalChannel,
                                         NULL,
                                         minVal,
                                         maxVal,
                                         DAQmx_Val_Volts,
                                         NULL );

  CreateInt32( 5, status, m, n, l );

  LhsVar(1) = 5;
  PutLhsVar();
  return 0;
}
/* ========================================================================== */