/* ========================================================================== */
/* Copyright (C) 2011 - DIGITEO - Allan CORNET */
/* ========================================================================== */
#include <NIDAQmx.h>
#include "stack-c.h"
#include "api_scilab.h"
#include "MALLOC.h"
#include "sciprint.h"
#include "Scierror.h"
#include "localization.h"
/* ========================================================================== */
static int convertStringToAttribute(const char *strAttribute);
/* ========================================================================== */
int sci_DAQ_SystemInfoAttribute(char* fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;
    char *pStrParamOne = NULL;
    int iAttribute = 0;

    CheckRhs(1, 1);
    
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (!isStringType(pvApiCtx, piAddressVarOne))
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: String expected.\n"), fname, 1);
        return 0;
    }

    if (!isScalar(pvApiCtx, piAddressVarOne))
    {
        Scierror(999,_("%s: Wrong size for input argument #%d: String expected.\n"), fname, 1);
        return 0;
    }

    if (getAllocatedSingleString(pvApiCtx, piAddressVarOne, &pStrParamOne) != 0)
    {
        Scierror(999,_("%s: Memory allocation error.\n"), fname);
        return 0;
    }

    iAttribute = convertStringToAttribute(pStrParamOne);
    freeAllocatedSingleString(pStrParamOne);
    pStrParamOne = NULL;

    switch (iAttribute)
    {
        case DAQmx_Sys_GlobalChans:
        case DAQmx_Sys_Scales:
        case DAQmx_Sys_Tasks:
        case DAQmx_Sys_DevNames:
            {
                char *infoStr = NULL;
                int sizeInfoStr = DAQmxGetSystemInfoAttribute (iAttribute, infoStr, 0);
                if (sizeInfoStr > 0)
                {
                    infoStr = (char*) MALLOC (sizeInfoStr *sizeof(char));
                    if (infoStr)
                    {
                        DAQmxGetSystemInfoAttribute(iAttribute, infoStr, sizeInfoStr);
                        createSingleString(pvApiCtx, Rhs + 1, infoStr);
                        LhsVar(1) = Rhs + 1;
                        PutLhsVar();
                        FREE (infoStr);
                    }
                    else
                    {
                        Scierror(999,_("%s: Memory allocation error.\n"), fname);
                        return 0;
                    }
                }
                else
                {
                    createEmptyMatrix(pvApiCtx, Rhs + 1);
                    LhsVar(1) = Rhs + 1;
                    PutLhsVar();
                }
            }
            break;

        case DAQmx_Sys_NIDAQMajorVersion:
        case DAQmx_Sys_NIDAQMinorVersion:
        case DAQmx_Sys_NIDAQUpdateVersion:
            {
                int iVersion = 0;
                int iErr = 0;
                iErr = DAQmxGetSystemInfoAttribute (iAttribute, &iVersion, 256);
                if (iErr != 0)
                {
                    createScalarDouble(pvApiCtx, Rhs + 1, (double) iVersion);
                    LhsVar(1) = Rhs + 1;
                    PutLhsVar();
                }
                else
                {
                    Scierror(999,_("%s: Can not read this data.\n"), fname);
                }
            }
            break;

        default:
            {
                Scierror(999,_("%s: Attribute not managed.\n"), fname);
            }
            break;
    }
    return 0;
}
/* ========================================================================== */
static int convertStringToAttribute(const char *strAttribute)
{
    /*
    if (strcmp(strAttribute, "DAQmx_Sys_GlobalChans") == 0) return DAQmx_Sys_GlobalChans;
    if (strcmp(strAttribute, "DAQmx_Sys_Scales") == 0) return DAQmx_Sys_Scales;
    if (strcmp(strAttribute, "DAQmx_Sys_Tasks") == 0) return DAQmx_Sys_Tasks;
    */
    if (strcmp(strAttribute, "DAQmx_Sys_DevNames") == 0) return DAQmx_Sys_DevNames;
    if (strcmp(strAttribute, "DAQmx_Sys_NIDAQMajorVersion") == 0) return DAQmx_Sys_NIDAQMajorVersion;
    if (strcmp(strAttribute, "DAQmx_Sys_NIDAQMinorVersion") == 0) return DAQmx_Sys_NIDAQMinorVersion;
    if (strcmp(strAttribute, "DAQmx_Sys_NIDAQUpdateVersion") == 0) return DAQmx_Sys_NIDAQUpdateVersion;
    return 0;
}
/* ========================================================================== */
