<?xml version="1.0" encoding="ISO-8859-1"?>
<refentry version="5.0-subset Scilab" xml:id="DAQ_Read_Functions"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>07-Oct-2011</pubdate>
  </info>

  <refnamediv>
    <refname>DAQ_Read_Functions</refname>

    <refpurpose>DAQ_ReadAnalogF64</refpurpose>
    <refpurpose>DAQ_ReadBinaryI16</refpurpose>
    <refpurpose>DAQ_ReadBinaryI32</refpurpose>
    <refpurpose>DAQ_ReadDigitalU8</refpurpose>
    <refpurpose>DAQ_ReadDigitalU32</refpurpose>
    <refpurpose>DAQ_ReadDigitalScalarU32</refpurpose>
    <refpurpose>DAQ_ReadCounterF64</refpurpose>
    <refpurpose>DAQ_ReadCounterScalarF64</refpurpose>
    <refpurpose>DAQ_ReadCounterScalarU32</refpurpose>
    <refpurpose>DAQ_ReadCounterU32</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[ readArray, sampsPerChanRead, status ] = DAQ_ReadAnalogF64 ( taskHandle, numSampsPerChan, timeout, fillMode, arraySizeInSamps )
[ readArray, sampsPerChanRead, status ] = DAQ_ReadBinaryI16 ( taskHandle, numSampsPerChan, timeout, fillMode, arraySizeInSamps )
[ readArray, sampsPerChanRead, status ] = DAQ_ReadBinaryI32 ( taskHandle, numSampsPerChan, timeout, fillMode, arraySizeInSamps )
[ readArray, sampsPerChanRead, status ] = DAQ_ReadDigitalU8 ( taskHandle, numSampsPerChan, timeout, fillMode, arraySizeInSamps )
[ readArray, sampsPerChanRead, status ] = DAQ_ReadDigitalU32 ( taskHandle, numSampsPerChan, timeout, fillMode, arraySizeInSamps )
[ value, status ] = DAQ_ReadDigitalScalarU32( taskHandle, timeout )
[ readArray, sampsPerChanRead, status ] = DAQ_ReadCounterF64 ( taskHandle, numSampsPerChan, timeout, arraySizeInSamps )
[ value, status ] = DAQ_ReadCounterScalarF64( taskHandle, timeout )
[ value, status ] = DAQ_ReadCounterScalarU32( taskHandle, timeout )
[ readArray, sampsPerChanRead, status ] = DAQ_ReadCounterU32 ( taskHandle, numSampsPerChan, timeout, arraySizeInSamps )</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>taskHandle</term>

        <listitem>
          <para>mlist (type "daq")</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>numSampsPerChan</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>timeout</term>

        <listitem>
          <para>double</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>fillMode</term>

        <listitem>
          <para>boolean</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>arraySizeInSamps</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>sampsPerChanRead</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>readArray</term>

        <listitem>
          <para>double, int16, int32, uint8 or uint32 row vector</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>value</term>

        <listitem>
          <para>double or uint32</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>status</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">DAQ_FunctionName</emphasis> provides just
        an interface to <emphasis
        role="bold">DAQmxFunctionName</emphasis>.</para>

        <para>For more detailed information about the interfaced functions,
        please refer to <emphasis>NI-DAQmx 9.x C Function Reference
        Help</emphasis>.</para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection>
    <title>Examples</title>

  <programlisting role="example">   Your NI-DAQmx 9.x installation contains a number of tutorial examples
   written in C, please refer to them.
   By default, they are located in   
   C:\Program Files\National Instruments\NI-DAQ\Examples (Windows).
  </programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="DAQ_Constants">DAQ_Constants</link></member>

      <member><link
      linkend="DAQ_Task_Configuration_and_Control">DAQ_Task_Configuration_and_Control</link></member>

      <member><link
      linkend="DAQ_Channel_Configuration_and_Creation">DAQ_Channel_Configuration_and_Creation</link></member>

      <member><link linkend="DAQ_Timing">DAQ_Timing</link></member>

      <member><link linkend="DAQ_Triggering">DAQ_Triggering</link></member>

      <member><link
      linkend="DAQ_Write_Functions">DAQ_Write_Functions</link></member>

      <member><link
      linkend="DAQ_Error_Handling">DAQ_Error_Handling</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <variablelist>
      <varlistentry>
        <term>Dirk Reusch</term>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>NI-DAQmx 9.x C Function Reference Help</para>
  </refsection>
</refentry>
