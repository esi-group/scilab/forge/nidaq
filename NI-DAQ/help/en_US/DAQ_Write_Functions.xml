<?xml version="1.0" encoding="ISO-8859-1"?>
<refentry version="5.0-subset Scilab" xml:id="DAQ_Write_Functions"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>07-Oct-2011</pubdate>
  </info>

  <refnamediv>
    <refname>DAQ_Write_Functions</refname>

    <refpurpose>DAQ_WriteAnalogF64</refpurpose>
    <refpurpose>DAQ_WriteDigitalU8</refpurpose>
    <refpurpose>DAQ_WriteDigitalU32</refpurpose>
    <refpurpose>DAQ_WriteDigitalScalarU32</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[ sampsPerChanWritten, status ] = DAQ_WriteAnalogF64 ( taskHandle, numSampsPerChan, timeout, dataLayout, writeArray )
[ sampsPerChanWritten, status ] = DAQ_WriteDigitalU8 ( taskHandle, numSampsPerChan, autoStart, timeout, dataLayout, writeArray )
[ sampsPerChanWritten, status ] = DAQ_WriteDigitalU32 ( taskHandle, numSampsPerChan, autoStart, timeout, dataLayout, writeArray )
[ status ] = DAQ_WriteDigitalScalarU32 ( taskHandle, autoStart, timeout, value )</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>taskHandle</term>

        <listitem>
          <para>mlist (type "daq")</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>numSampsPerChan</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>autoStart</term>

        <listitem>
          <para>boolean</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>timeout</term>

        <listitem>
          <para>double</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>value</term>

        <listitem>
          <para>uint32</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>dataLayout</term>

        <listitem>
          <para>boolean</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>writeArray</term>

        <listitem>
          <para>double, uint8 or uint32 row vector</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>sampsPerChanWritten</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>status</term>

        <listitem>
          <para>integer</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">DAQ_FunctionName</emphasis> provides just
        an interface to <emphasis
        role="bold">DAQmxFunctionName</emphasis>.</para>

        <para>For more detailed information about the interfaced functions,
        please refer to <emphasis>NI-DAQmx 9.x C Function Reference
        Help</emphasis>.</para>
      </listitem>
    </itemizedlist>
  </refsection>

  <refsection>
    <title>Examples</title>

  <programlisting role="example">   Your NI-DAQmx 9.x installation contains a number of tutorial examples
   written in C, please refer to them.
   By default, they are located in   
   C:\Program Files\National Instruments\NI-DAQ\Examples (Windows).
  </programlisting>
  
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="DAQ_Constants">DAQ_Constants</link></member>

      <member><link
      linkend="DAQ_Task_Configuration_and_Control">DAQ_Task_Configuration_and_Control</link></member>

      <member><link
      linkend="DAQ_Channel_Configuration_and_Creation">DAQ_Channel_Configuration_and_Creation</link></member>

      <member><link linkend="DAQ_Timing">DAQ_Timing</link></member>

      <member><link linkend="DAQ_Triggering">DAQ_Triggering</link></member>

      <member><link
      linkend="DAQ_Error_Handling">DAQ_Error_Handling</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <variablelist>
      <varlistentry>
        <term>Dirk Reusch</term>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>NI-DAQmx 9.x C Function Reference Help</para>
  </refsection>
</refentry>
